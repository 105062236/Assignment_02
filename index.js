var lastTime = 0;
var mainState = {

    
    preload: function() {

        //game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'assets/player.png', 32, 32); // 主角
        game.load.image('wall', 'assets/wall.png'); // 牆壁
        game.load.image('ceiling', 'assets/ceiling.png'); // 天花板的刺 
        game.load.image('normal', 'assets/normal.png'); // 藍色平台
        game.load.image('nails', 'assets/nails.png'); // 帶刺平台
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16); // 向右捲動的平台
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16); // 向左捲動的平台
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22); // 彈簧墊
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36); // 翻轉的大平台
        game.load.image('speedup', 'assets/speedup_resized.png');//加速圖片
        game.load.image('speeddown', 'assets/speeddown_resized.png');//減速圖片
        game.load.image('bg', 'assets/playbg.jpg');
        
        
        //sound
        game.load.audio('normal','assets/normalstair.mp3');
        game.load.audio('trampoline','assets/trampolinesound.mp3');
        game.load.audio('fake','assets/fakesound.mp3');
        game.load.audio('dead','assets/deadsound.mp3');
        game.load.audio('conveyor','assets/conveyersound.mp3');
        game.load.audio('speed','assets/speed.mp3');
        game.load.audio('bgsound','assets/bgsound.mp3');
        
    },

    create: function() {
            
        
        
        game.add.sprite(0, 0, 'bg'); 

        //map rate
        this.rate = 1;

        //walls

        this.walls = game.add.group(); 
        this.walls.enableBody = true; 
        
        
        this.leftwall = game.add.sprite(0, 0, 'wall', 0, this.walls); 
        this.rightwall = game.add.sprite(415, 0, 'wall', 0, this.walls); 

        this.walls.setAll('body.immovable', true);
        game.physics.arcade.enable(this.walls);
    
        //platform 

        this.platforms = game.add.group();
        this.platforms.enableBody = true;
        

        game.add.sprite(100, 100, 'normal', 0, this.platforms);
        game.add.sprite(210, 150, 'normal', 0, this.platforms);
        game.add.sprite(300, 200, 'normal', 0, this.platforms);
    
        this.platforms.setAll('body.immovable', true);
        game.physics.arcade.enable(this.platforms);

        


        //nails
        this.nails = game.add.group();
        this.nails.enableBody = true;
        this.nails.setAll('body.immovable', true);
        game.physics.arcade.enable(this.nails);

        //conveyorRight
        this.conveyorRight = game.add.group();
        this.conveyorRight.enableBody = true;

        //conveyorLeft
        this.conveyorLeft = game.add.group();
        this.conveyorLeft.enableBody = true;

        //trampoline
        this.trampoline = game.add.group();
        this.trampoline.enableBody = true;

        //fake
        this.fake = game.add.group();
        this.fake.enableBody = true;

        //ceiling
        this.ceiling = game.add.sprite(15, 0, 'ceiling');

        //speedup
        this.speedup = game.add.group();
        this.speedup.enableBody = true;

        //speeddown
        this.speeddown = game.add.group();
        this.speeddown.enableBody = true;

        //player
        this.player = game.add.sprite(game.width/2, 20, 'player');
        this.player.anchor.setTo(0.5, 0.5); 
        this.player.enableBody = true;
        game.physics.arcade.enable(this.player);

        this.player.animations.add('left', [0, 1, 2, 3], 20);
        this.player.animations.add('right', [9, 10, 11, 12], 20);
        this.player.animations.add('jump', [36, 37, 38, 39], 20);
        this.player.animations.add('danger', [37, 17, 40], 20);
        this.player.animations.add('dangerLeft', [4, 6, 22, 24], 20);
        this.player.animations.add('dangerRight', [13, 15, 31, 33], 20);

        this.player.body.velocity.y = 50;
        this.player.body.gravity.y = 300;
        this.player.speedchage = 0;

        this.player.touch = undefined;
        this.player.life = 10;
        this.player.unbeatableTime = 500 + this.time.now;
        this.player.hurt = false;

        this.player.lifeLabel = game.add.text(15, 15, 'life: 10 stair: 0', { font: '18px Arial', fill: 'red' });

        //sound

        this.player.normal = game.add.audio('normal');
        this.player.fake = game.add.audio('fake');
        this.player.trampoline = game.add.audio('trampoline');
        this.player.dead = game.add.audio('dead');
        this.player.conveyor = game.add.audio('conveyor');
        this.player.speed = game.add.audio('speed');
        this.player.bgsound = game.add.audio('bgsound');
        this.player.bgsound.loop = true;
        this.player.bgsound.play();

        //count the stair
        this.player.stair = 0;

        this.cursor = game.input.keyboard.createCursorKeys();

        //speedup
    },


    update: function() {
        //this.player.body.x += 1;
        game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.player, this.platforms);
        game.physics.arcade.collide(this.player, this.nails);
        game.physics.arcade.collide(this.player, this.conveyorLeft);
        game.physics.arcade.collide(this.player, this.conveyorRight);
        game.physics.arcade.collide(this.player, this.fake);
        game.physics.arcade.collide(this.player, this.trampoline);
        
        
        
        this.platforms.forEachAlive(this.updatePlatform,this);
        this.nails.forEachAlive(this.updatePlatform,this);
        this.conveyorRight.forEachAlive(this.updatePlatform,this);
        this.conveyorLeft.forEachAlive(this.updatePlatform,this);
        this.trampoline.forEachAlive(this.updatePlatform,this);
        this.fake.forEachAlive(this.updatePlatform,this);

        this.speedup.forEachAlive(this.updatePlatform,this);
        this.speeddown.forEachAlive(this.updatePlatform,this);

        
        this.creatPlatforms();

        game.physics.arcade.collide(this.player, this.platforms, this.effect);
        game.physics.arcade.collide(this.player, this.nails, this.effect);
        game.physics.arcade.collide(this.player, this.conveyorRight, this.effect);
        game.physics.arcade.collide(this.player, this.conveyorLeft, this.effect);
        game.physics.arcade.collide(this.player, this.trampoline, this.effect);
        game.physics.arcade.collide(this.player, this.fake, this.effect);

        game.physics.arcade.collide(this.player, this.ceiling, this.effect);

        game.physics.arcade.collide(this.player, this.speedup, this.special);
        game.physics.arcade.collide(this.player, this.speeddown, this.special);
        this.movePlayer();
        
        this.showLife();

        if (!this.player.inWorld){
            this.player.life = 0;
            this.player.dead.play();
        }

        if(this.player.life <　1) {
            this.recordScore(this.player.stair);
            this.player.bgsound.pause();
            game.state.start('dead');
        }    
       
    },
    special: function(player, special){
        if(special.key == 'speedup'){
            player.speed.play();
            player.speedchage = 150;
            setTimeout(function() {
                player.speedchage = 0;
            }, 3000);
        }
        else if(special.key == 'speeddown'){
            player.speed.play();
            player.speedchage = -80;
            setTimeout(function() {
                player.speedchage = 0;
            }, 3000);
        }
        special.body.checkCollision.none = true;
        special.kill();
    },

    recordScore: function(stair){
        var person = prompt("Please enter your name", "Rafa Nadal");
        if (person != null) {
            var ref = firebase.database().ref('rank');

            ref.once('value').then(function(snapshot){
                var set = {
                    name : person,
                    score: stair
                };
                var newpost = ref.push(set);
                person = '';
            })
        }
    },
    
    showLife: function(){
        this.player.lifeLabel.text = 'life: ' + this.player.life + ' stair: ' + this.player.stair; 
    },
    
    effect: function(player, platform){
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        if(platform.key == 'conveyorLeft') {
            player.body.x -= 1;
            if(player.touch !== platform) {
                player.conveyor.play();
                player.stair++;
                if(player.life < 10) {
                    player.life += 1;
                }
                player.touch = platform;
            }
        }
        if(platform.key == 'conveyorRight') {
            player.body.x += 1;
            if(player.touch !== platform) {
                player.conveyor.play();
                player.stair++;
                if(player.life < 10) {
                    player.life += 1;
                }
                player.touch = platform;
            }
        }
        if(platform.key == 'trampoline') {
            setTimeout(function() {
                player.trampoline.play();
            }, 300);
            if(player.touch !== platform) {  
                player.stair++;
                player.touch = platform;
            }
            player.body.velocity.y = -200;
            platform.play('jump');
            
        }
        if(platform.key == 'nails') {
            if(player.touch !== platform) {
                player.dead.play();
                player.stair++;
                player.life -= 2;
                player.touch = platform;
                player.unbeatableTime = game.time.now + 1500;
            }
        }
        if(platform.key == 'normal') {
            
            if(player.touch !== platform) {
                player.normal.play();
                player.stair++;
                if(player.life < 10) {
                    player.life += 1;
                }
                player.touch = platform;
            }
        }
        if(platform.key == 'fake') {
           if(player.touchOn !== platform) {
                player.stair++;
                setTimeout(function() {
                    player.fake.play();
                    platform.play('turn');
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
            
        }
        
    },

    creatPlatforms: function(){
        
            
        if(game.time.now > lastTime + (1100 / this.rate)) {
            lastTime = game.time.now;
            var x = Math.random();
            var xx = Math.random();
            var xxx = Math.random();
            var pp, special;

            //special
            if(xxx < 0.15){
                special = game.add.sprite(xx * 330 , 380, 'speedup', 0, this.speedup);
                this.speedup.setAll('body.immovable', true);
                game.physics.arcade.enable(this.speedup);
            }
            else if(xxx < 0.3){
                special = game.add.sprite(xx * 330, 380, 'speeddown', 0, this.speeddown);
                this.speeddown.setAll('body.immovable', true);
                game.physics.arcade.enable(this.speeddown);
            }
            
            if(x < 0.1){
                pp = game.add.sprite(xx * 330, 410, 'nails', 0, this.nails);
                pp.body.setSize(96, 15, 0, 15);
                this.nails.setAll('body.immovable', true);
                game.physics.arcade.enable(this.nails);
            }
            else if(x < 0.25){
                pp = game.add.sprite(xx * 330, 410, 'conveyorRight', 0, this.conveyorRight);
                pp.body.setSize(96, 1, 0, 0);
                pp.animations.add('scroll', [0, 1, 2, 3], 8, true);
                pp.animations.play('scroll');
                this.conveyorRight.setAll('body.immovable', true);
                game.physics.arcade.enable(this.conveyorRight);

                
            }
            else if(x < 0.4){
                pp = game.add.sprite(xx * 330, 410, 'conveyorLeft', 0, this.conveyorLeft);
                pp.body.setSize(96, 1, 0, 0);
                pp.animations.add('scroll', [0, 1, 2, 3], 8, true);
                pp.animations.play('scroll');
                this.conveyorLeft.setAll('body.immovable', true);
                game.physics.arcade.enable(this.conveyorLeft);
            }
            else if(x < 0.55){
                pp = game.add.sprite(xx * 330, 410, 'trampoline', 0, this.trampoline);
                pp.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                pp.frame = 3;
                this.trampoline.setAll('body.immovable', true);
                game.physics.arcade.enable(this.trampoline);
            }
            else if(x < 0.7){
                pp = game.add.sprite(xx * 330, 410, 'fake', 0, this.fake);
                pp.animations.add('turn', [1, 2, 3, 4, 5, 0], 8, false);
                pp.frame = 0;
                this.fake.setAll('body.immovable', true);
                game.physics.arcade.enable(this.fake);
            }
            else{   
                pp = game.add.sprite(xx * 330, 410, 'normal', 0, this.platforms);
                this.platforms.setAll('body.immovable', true);
                game.physics.arcade.enable(this.platforms);
            }
            
            
        }
    },

    updatePlatform: function(child){
   
        child.body.position.y -= this.rate;
        if(this.player.stair == 10) this.rate = 1.4;
        if(this.player.stair == 20) this.rate = 1.8;
        if(this.player.stair == 30) this.rate = 2;
        if(this.player.stair == 40) this.rate = 2.5;


       


    },

    movePlayer: function() {


        if (this.cursor.left.isDown){
            this.player.body.velocity.x = -120 - this.player.speedchage;
            if(game.time.now < this.player.unbeatableTime && this.player.life != 10) this.player.animations.play('dangerLeft');
            else this.player.animations.play('left');
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 120 + this.player.speedchage;
            if(game.time.now < this.player.unbeatableTime　&& this.player.life != 10) this.player.animations.play('dangerRight');
            else this.player.animations.play('right');
        }
        else{
            this.player.body.velocity.x = 0;  
            if(this.player.body.velocity.y != 0){
                if(game.time.now < this.player.unbeatableTime　&& this.player.life != 10) this.player.animations.play('danger');
                else this.player.animations.play('jump');
            }
            else{
                if(game.time.now < this.player.unbeatableTime　&& this.player.life != 10) this.player.animations.play('danger');
                else this.player.frame = 8;
            }
        }

        //touch ceiling
        if(this.player.body.y <= 10){
            
            if(this.player.body.velocity.y !== 0) {
                this.player.body.velocity.y = 0;
                
            }
            if(game.time.now > this.player.unbeatableTime) {
                this.player.dead.play();
                this.player.life -= 3;
                this.player.animations.play('danger');
                //this.player.frame = 5;
                this.player.unbeatableTime = game.time.now + 1000;
            }
            this.player.body.y = 15;
            
        }
        
        
    }

        
};
var color = 'red';
var manuState = {
    preload: function(){
        game.load.image('menubg','assets/menubg.jpg', 430, 400);
    },
    create: function(){
        this.bg = game.add.sprite(0,0, 'menubg');
        this.bg.scale.setTo(0.1,0.12);
        this.keyboard = game.input.keyboard.addKeys({
            'Enter': Phaser.Keyboard.ENTER,
            'Space': Phaser.Keyboard.SPACEBAR
        });
    },
    update: function(){
        var time = game.time.now;
        this.beg = game.add.text(75, 150, 'Press Enter to start', { font: '18px Arial', fill: color });
        this.scoreboard = game.add.text(75, 270, 'Press Space to see scoreboard', { font: '18px Arial', fill: '#ffffff' });
        this.beg.scale.setTo(1.5, 1.5);
        if(this.time.now % 1000 < 15){
            color ++; 
        }
        else if(this.time.now % 1000 > 900){
            color = 'red';
        }
        

        if(this.keyboard.Enter.isDown){
            game.state.start('main');
        }
        if(this.keyboard.Space.isDown){
            game.state.start('scoreboard');
        }
    }
}

var deadState = {
    preload: function(){
        game.load.image('deadbg','assets/deadbg.jpg');
    },
    create: function(){
        color = 'green';
        this.bg = game.add.sprite(0,0, 'deadbg');
        this.bg.scale.setTo(0.1,0.12);
        this.keyboard = game.input.keyboard.addKeys({
            'Enter': Phaser.Keyboard.ENTER ,
            'Space': Phaser.Keyboard.SPACEBAR
        });
    },
    update: function(){
        var time = game.time.now;
        this.dead = game.add.text(120, 30, 'YOU  DEAD', { font: '30px Arial', fill: color });
        this.beg = game.add.text(60, 150, 'Press Enter to play again', { font: '18px Arial', fill: '#ffffff' });
        this.scoreboard = game.add.text(75, 270, 'Press Space to see scoreboard', { font: '18px Arial', fill: '#ffffff' });
        this.beg.scale.setTo(1.5, 1.5);

        if(this.time.now % 1000 < 15){
            color ++; 
        }
        else if(this.time.now % 1000 > 980){
            color = 'green';
        }
        

        if(this.keyboard.Enter.isDown){
            game.state.start('main');
        }
        if(this.keyboard.Space.isDown){
            game.state.start('scoreboard');
        }
        
    }
}

var scoreState = {
    preload: function(){
        game.load.image('scoreboard','assets/scoreboardbg.jpg');
    },
    create: function(){
        color = 'green';
        this.bg = game.add.sprite(0,0, 'scoreboard');
        this.bg.scale.setTo(0.1,0.12);
        this.keyboard = game.input.keyboard.addKeys({
            'Enter': Phaser.Keyboard.ENTER
        });

        this.beg = game.add.text(50, 350, 'Press Enter to return manu', { font: '18px Arial', fill: '#ff0000' });
        
        var ref = firebase.database().ref("rank");
        
        var bb = '';
        var rank = 0;
        var ranking = game.add.text(50, 30, '', { font: 'bold 18px sans-serif', fill: '#FF4D00' });
        ref.once('value').then(function (snapshot) {
            rank = snapshot.numChildren();
            if(rank > 10) rank = 10;
        })
        .then(function(){
            ref.orderByChild("score").on("child_added", function(snapshot) {
                ranking.text = rank + '. ' + snapshot.val().name + ' ----- ' + snapshot.val().score  + '\n' + ranking.text;
                rank--;
            })
            
        })
        

    },
    update: function(){

        if(this.keyboard.Enter.isDown){
            game.state.start('manu');
        }
    }
}
var game = new Phaser.Game(430, 400, Phaser.AUTO, 'canvas');
game.state.add('manu', manuState);
game.state.add('scoreboard', scoreState);
game.state.add('main', mainState);
game.state.add('dead', deadState);
game.state.start('manu');

    
    